package problema2;

import java.util.Arrays;
import java.util.Scanner;

public class Problema {
	 public static void main(String[] args) {
		 		 
		  	Scanner userInput = new Scanner(System.in);
			System.out.println("Introduceti a= ");
			a = userInput.nextInt();
			System.out.println("Introduceti b= ");
			b = userInput.nextInt();
			init(a,b);
			
		 		 
	        
	    }
		
		private static void init(int a,int b){
		int[] arr= new int[100];
	        int r;
	    
	        
		 	int a,b,i,j,k;
		 	int divNr=0,div=0,indice=0;
		for (i = a; i <= b; i++) {
				divNr=0;
				div=0;
				indice=0;
				
				for(j=2;j<i;j++)
					if(i%j==0)
						divNr++;
				if(divNr==0){  //daca numarul este prim trecem la calculul sumei
					for(k=2;k<=i;i++){
						for(j=2;j<k;j++)
							if(k%j==0)
								div++;
						if(div==0){  //daca termenul din suma este prim il adaug in vector
							arr[indice]=k;
							indice++;
						}
							
					}
					//cream combinatiile posibile
					r=3;
					combina(arr, r);
					
				}
				
					
				
			}
			}

	    private static void combina(int[] arr, int r) {
	        int[] res = new int[r];
	        Combinari(arr, res, 0, 0, r);
	    }



	    private static void Combinari(int[] arr, int[] res, int currIndex, int level, int r) {
	        if(level == r){
	            printArray(res);
	            return;
	        }
	        for (int i = currIndex; i < arr.length; i++) {
	            res[level] = arr[i];
	            Combinari(arr, res, i+1, level+1, r);
	            //evit daca apar duplicate
	            if(i < arr.length-1 && arr[i] == arr[i+1]){
	                i++;
	            }
	        }
	    }

	    private static void printArray(int[] res) {
	        for (int i = 0; i < res.length; i++) {
	            System.out.print(res[i] + " ");
	        }
	        System.out.println();
	    }

}
//am determinat deocamdata doar numerele prime din intervalul [a,b] si care sunt subtermenii primi care pot alcatui suma nr din interval
